	exports.definition = {
    config : {
        // table schema and adapter information
        "columns": {
        	"rowid": "INTEGER",
            "tags": "TEXT",
            "description": "TEXT",
            "example":"TEXT",
            "url":"TEXT"
        },
  
        "adapter": {
            "type": "sql",
            "collection_name": "duckinfo",
             "db_name":"duck",
            "db_file":"duck.db",
             // "idAttribute": "rowid"
            
        }
    }
 };